import { useEffect } from "react";
import { useDispatch } from "react-redux";
import { Routes, Route, Navigate } from "react-router-dom";

import { onAuthStateChangedListener, createUserDocumentFromAuth } from "./firebase/firebase";
import Home from "./routes/home/home.component";
import Authentication from "./routes/authentication/authentication.component.jsx";
import Shop from "./routes/shop/shop.component";
import Checkout from "./routes/checkout/checkout.component";
// import Category from "./routes/category/category.component";
import ProductDetails from "./components/product-details/product-details.component";
// import CategoriesPreview from "./routes/categories-preview/categories-preview.component";
import { setCurrentUser } from "./store/user/user.action";

const App = () => {
  const dispatch = useDispatch();
  useEffect(() => {
    const unsubscribe = onAuthStateChangedListener((user) => {
      if (user) {
        createUserDocumentFromAuth(user);
      }
      dispatch(setCurrentUser(user));
    });

    return unsubscribe;
  }, [dispatch]);//this dispatch never changes, but because of the ESlint warning we can add it

  return (
    <Routes>
      <Route index element={<Navigate to='/home' />} />
      <Route path="home" element={<Home />} />
      <Route path="shop/*" element={<Shop/>}/>
      <Route path="product-details/:id" element={<ProductDetails />} />
      <Route path="auth" element={<Authentication />} />
      <Route path="checkout" element={<Checkout />} />
    </Routes>
  )
}

export default App;
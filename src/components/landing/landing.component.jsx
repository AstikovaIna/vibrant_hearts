import "./landing.styles.jsx";
import picture from "../../assets/pexels-yan-krukov-6210191.jpg";

import {
  Description,
  HorizontalLine,
  Picture,
  LandingContainer,
} from "./landing.styles.jsx";

const Landing = () => {
  const scroll = () => {
    window.scrollTo(0, 1600);
  };
  return (
    <LandingContainer>
      <Picture>
        <img src={picture} alt="landing-img"/>
      </Picture>
      <Description>
        <h6>We are your best choice for an authentic gift.</h6>
        <h1>
          Bring nature to your home or send it with love to someone special.
        </h1>
        <h5>
          Vibrant Hearts studio is born from love to art, natural materials,
          authentic and humble vision about everyday lifestyle.
        </h5>
        <HorizontalLine onClick={scroll}>Explore our ideas</HorizontalLine>
      </Description>
    </LandingContainer>
  );
};

export default Landing;

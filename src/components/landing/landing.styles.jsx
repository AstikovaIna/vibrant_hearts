import styled from "styled-components";

export const Description = styled.div`
  width: 80vw;
  padding: 25px;
  flex-direction: row;
  flex-wrap: wrap;
  justify-content: space-between;
  // height: 500px;

  h1 {
    font-weight: 500;
    font-size: 2.8vw;
    padding-top: 35px;
  }

  h5 {
    font-weight: 300;
    font-size: 2.2vw;
    padding-top: 35px;
  }

  h6 {
    font-weight: 400;
    font-size: 2.2vw;
    color: #f76e11;
  }

  @media (max-width: 820px) {
    width: 90vw;
    h1{
      font-size: 3vw;
    }
    h5,h6{
      font-size: 2.5vw;
    }
  }
`;

export const HorizontalLine = styled.h3`
  margin-left: 60px;
  font-weight: 400;
  font-size: 18px;
  cursor: pointer;
  padding-top: 25px;

  &:before {
    content: "";
    display: flex;
    height: 2px;
    width: 45px;
    position: relative;
    text-align: center;
    justify-content: center;
    top: 13px;
    left: -60px;
    background: #f76e11;
    transition: 0.5s all ease 0.5s;
    -webkit-transition: 0.5s all ease 0s;
  }

  &:hover {
    cursor: pointer;
    transform: translateX(8px);
    transition: 0.5s all ease 0s;
    -webkit-transition: 0.5s all ease 0s;
  }
  @media (max-width: 768px) {
    display: none;
  }
`;

export const Picture = styled.div`
  &:hover {
    transform: scale(1.1);
    transition: transform 6s cubic-bezier(0.25, 0.45, 0.45, 0.95);
  }
  img {
    display: flex;
    align-items: center;
    justify-content: center;
    width: 80vw;
    background-size: cover;
    border-radius: 42% 58% 70% 30% / 34% 36% 64% 66%;
  }
`;
export const LandingContainer = styled.div`
  width: 95vw;
  border: solid #f7ecde 3px;
  border-radius: 71% 29% 57% 43% / 62% 56% 44% 38%;
  display: flex;
  flex-direction: row;
  justify-content: space-around;
  padding: 25px;
  margin: 40px 0px 50px 0px;
  @media (max-width: 820px) {
    flex-direction: column;
    justify-content: center;
    border-radius: 61% 39% 28% 72% / 37% 30% 70% 63%;
  }
  @media (max-width: 680px) {
    margin: 10px;
  }
`;

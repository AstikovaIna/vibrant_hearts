import styled from "styled-components";

import {SpinnerContainer} from "../spinner/spinner.styles";

export const BaseButton = styled.button`
  min-width: 165px;
  width: auto;
  height: 50px;
  letter-spacing: 0.5px;
  line-height: 50px;
  padding: 0 35px 0 35px;
  font-size: 13px;
  background-color: black;
  color: white;
  // text-transform: uppercase;
  font-family: "Open Sans Condensed";
  font-weight: bolder;
  border: none;
  cursor: pointer;
  display: flex;
  justify-content: center;
  align-items: center;
  border-radius: 33% 67% 65% 35% / 35% 49% 51% 65%;
  &:hover {
    background-color: white;
    color: black;
    border: 1px solid black;
  }
`;

export const GoogleButton = styled(BaseButton)`
  background-color: #4285f4;
  color: white;
  align-items: center;
  padding: 5px;
  border-radius: 74% 26% 65% 35% / 56% 49% 51% 44%;
  &:hover {
    background-color: #357ae8;
    border: none;
  }
`;

export const InvertedButton = styled(BaseButton)`
  background-color: white;
  color: black;
  border: 1px solid black;
  // border-radius: 33% 67% 65% 35% / 35% 49% 51% 65%;
  &:hover {
    background-color: black;
    color: white;
    border: none;
  }
`;

export const ButtonSpinner = styled(SpinnerContainer)`
width: 30px;
height: 30px;
`
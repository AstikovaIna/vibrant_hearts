import { useSelector } from "react-redux";
import { selectCurrentUser } from "../../store/user/user.selector";
import logo from "../../assets/VH-logo.svg";
import { signOutUser } from "../../firebase/firebase.js";
import CartDropdown from "../cart-dropdown/cart-dropdown.component";
import { selectIsCartOpen } from "../../store/cart/cart.selector";
import {
  NavContainer,
  LogoContainer,
  NavLink,
  NavLinks,
  ShoppingCart,
  HelloUser,
} from "./navigation.styles.jsx";
import CartIcon from "../cart-icon/cart-icon.components";
import BurgerMenu from "../mobile-menu/mobile.menu.component";

const Navigation = () => {
  
  const currentUser = useSelector(selectCurrentUser)
  const  isCartOpen  = useSelector(selectIsCartOpen);

  const scroll = () => {
    window.scrollTo(0, document.body.scrollHeight);
  };
  const userEmailName = currentUser?.email?.split("@")[0];

  return (
    <>
      <BurgerMenu />
      <NavContainer>
        <LogoContainer to="/">
          <img src={logo} alt="vh-logo" className="logo" />
        </LogoContainer>

        <NavLinks>
          <NavLink to="/shop">Shop</NavLink>
          <NavLink to="/about">About</NavLink>
          <NavLink as="p" onClick={scroll}>
            Contact
          </NavLink>
          {currentUser ? (
            <>
              <NavLink onClick={signOutUser}>Sign out</NavLink>
            </>
          ) : (
            <NavLink to="/auth">Sign in</NavLink>
          )}
        </NavLinks>

        <ShoppingCart>
          <CartIcon />

          {isCartOpen && <CartDropdown />}
          {currentUser && (
            <HelloUser>
              {currentUser?.displayName ? (
                <p>Hi, {currentUser.displayName}!</p>
              ) : (
                <p>Hi, {userEmailName}!</p>
              )}
            </HelloUser>
          )}
        </ShoppingCart>
      </NavContainer>
    </>
  );
};

export default Navigation;

import styled from "styled-components";
import { Link } from "react-router-dom";

export const NavContainer = styled.div`
  // width: 90vw;
  height: 70px;
  display: flex;
  justify-content: space-between;
  // margin-right: 25px;
  @media (max-width: 768px) {
    display: none;
  }
`;

export const LogoContainer = styled(Link)`
  height: 100%;
  width: 150px;
  margin-top: 10px;

  .logo {
    width: 140px;
    height: 45px;
  }
`;

export const NavLinks = styled.div`
  height: 100%;
  display: flex;
  font-weight: 300;
  align-items: center;
  justify-content: center;
  // background-color: #f7ecde;
  // border-radius: 63% 37% 71% 29% / 52% 56% 44% 48%;
`;

export const NavLink = styled(Link)`
  padding: 10px 15px;
  cursor: pointer;
  &:hover {
    color: #f76e11;
  }
`;

export const ShoppingCart = styled.div`
  display: flex;
  align-items: center;
`;

export const HelloUser = styled.div`
  display: flex;
  align-items: center;
  p {
    font-weight: 400;
    font-size: 16px;
    // color: #f76e11;
  }
`;

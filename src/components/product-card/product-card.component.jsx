import { useDispatch, useSelector } from "react-redux";
import { selectCartItems } from "../../store/cart/cart.selector.js";
import { addItemToCart } from "../../store/cart/cart.action.js";
import { useNavigate } from "react-router-dom";
import "./product-card.styles.jsx";
import Button from "../button/button.component";
import { BUTTON_TYPE_CLASSES } from "../../utils/button-types";
import {
  Name,
  Price,
  Footer,
  CardContainer,
  ButtonContainer,
} from "./product-card.styles.jsx";

const ProductCard = ({ product, category }) => {
  const { name, price, imageUrl, id } = product;
  const dispatch = useDispatch();
  const cartItems = useSelector(selectCartItems);
  const addProductToCart = () => dispatch(addItemToCart(cartItems, product));

  const navigate = useNavigate();
  const goToProductDetails = () => {
    navigate(`/product-details/${id}`);
  };
  return (
    <CardContainer>
      <img src={imageUrl} alt={`${name}`} onClick={goToProductDetails} />
      <Footer>
        <Name onClick={goToProductDetails}>{name}</Name>
        <Price>Price: ${price}</Price>
      </Footer>
      <ButtonContainer>
        <Button
          onClick={addProductToCart}
          buttonType={BUTTON_TYPE_CLASSES.inverted}
        >
          Add to cart
        </Button>
      </ButtonContainer>
    </CardContainer>
  );
};

export default ProductCard;

import styled from "styled-components";
import { randomRadius } from "../../utils/border-radius";

export const Name = styled.h3`
  width: 90%;
  margin-top: 15px;
  font-weight: 350;
  margin-bottom: 15px;
  cursor: pointer;
`;
export const Price = styled.span`
  width: 10%;
`;
export const Footer = styled.div`
  width: 90%;
  height: 5%;
  display: flex;
  // gap: 3px
  // justify-content: space-around;
  font-size: 16px;
  margin-top: 10px;
  align-items: center;
`;

export const ButtonContainer = styled.div`
  width: 80%;
  opacity: 0.7;
  position: absolute;
  top: 255px;
  display: flex;
  justify-content: center;
  @media (hover: hover) {
    opacity: 0.85;
  }
`;
export const CardContainer = styled.div`
  width: 100%;
  display: flex;
  flex-direction: column;
  height: 350px;
  align-items: center;
  position: relative;

  img {
    width: 100%;
    height: 95%;
    object-fit: cover;
    // margin-bottom: px;
    border-radius: ${randomRadius};
    cursor: pointer;
  }

  &:hover {
    img {
      opacity: 0.8;
    }
  }
`;

import { useParams } from "react-router-dom";
import { useSelector, useDispatch} from "react-redux";
import { selectCartItems } from "../../store/cart/cart.selector.js";
import { addItemToCart } from "../../store/cart/cart.action.js";
import {
  TitleContainer,
  Image,
  Details,
  Info,
  PriceAndButton,
  DetailsContainer,
} from "./product-details.style.jsx";

import { selectCategoriesMap } from "../../store/categories/category.selector.js";
import Navigation from "../navigation/navigation.component";
import Button from "../button/button.component";
import { BUTTON_TYPE_CLASSES } from "../../utils/button-types";
import InfoSection from "../info.section/info.section.component";
import Footer from "../footer/footer.component";

const ProductDetails = () => {
  const categoriesMap = useSelector(selectCategoriesMap);
  const productId = useParams();
  const dispatch = useDispatch();
  const cartItems = useSelector(selectCartItems);

  const allProducts = Object.values(categoriesMap);
  const product = allProducts
    .map((arr) => arr.filter((item) => item.id.toString() === productId.id))
    .filter((arr) => arr.length > 0);
  const { name, imageUrl, price } = product[0][0];
 
  const addProductToCart = () => dispatch(addItemToCart(cartItems, product[0][0]));
  return (
    <>
      <Navigation />
      <TitleContainer>
        <h1>{name}</h1>
        <div></div>
      </TitleContainer>
      <DetailsContainer>
        <Details>
          <Image>
            <img src={imageUrl} alt={name} />
          </Image>
          <PriceAndButton>
            <h3>Price: ${price}</h3>

            <Button
              onClick={addProductToCart}
              buttonType={BUTTON_TYPE_CLASSES.inverted}
            >
              Add to cart
            </Button>
          </PriceAndButton>
        </Details>
        <Info>
          <h4>More information: </h4>
          <span>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam congue
            tincidunt ante at luctus. Duis ac risus dignissim, consectetur ante
            ac, feugiat nunc. Nulla facilisi. Fusce fermentum eget odio eget
            molestie. Pellentesque habitant morbi tristique senectus et netus et
            malesuada fames ac turpis egestas. In at sapien vel libero commodo
            hendrerit in in lacus. Maecenas vestibulum, nunc eget posuere
            faucibus, magna leo sollicitudin lacus, non tincidunt quam est in
            odio.
          </span>
        </Info>
      </DetailsContainer>
      <InfoSection />
      <Footer />
    </>
  );
};

export default ProductDetails;

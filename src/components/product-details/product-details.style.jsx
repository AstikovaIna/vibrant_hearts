import styled from "styled-components";

export const TitleContainer = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  flex-direction: column;
  margin-bottom: 35px;
  div {
    width: 70px;
    height: 2.5px;
    background-color: #f76e11;
  }
  h1 {
    font-size: 28px;
    font-weight: 500;
    margin-top: 40px;
    margin-left: 25px;
  }
`;
export const Image = styled.div`
  img {
    width: 300px;
    height: 250px;
    transform-origin: 0 0;
    transition: transform 2s, visibility 2s ease-in-out;
  }
  img:hover {
    border-radius: 0.8rem;
    transform: scale(1.5);
    cursor: zoom-in;
  }
`;
export const Details = styled.div`
  width: 40%;
  @media (max-width: 640px) {
    width: 85vw;
  }
`;

export const Info = styled.div`
  width: 50%;
  @media (max-width: 640px) {
    width: 85vw;
    margin-top: 20px;
  }
`;
export const PriceAndButton = styled.div`
  display: flex;
  justify-content: space-around;
  align-items: center;
`;
export const DetailsContainer = styled.div`
  display: flex;
  margin: 35px 25px 15px 25px;
  justify-content: space-around;
  img {
    border-radius: 42% 58% 70% 30% / 23% 49% 51% 77%;
  }
  @media (max-width: 640px) {
    display: block;
    align-items: center;
  }
`;

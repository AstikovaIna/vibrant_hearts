import {
  BackgroundImage,
  Body,
  DirectoryItemContainer,
} from "./directory-item.styles.jsx";
import { useNavigate } from "react-router-dom";

const DirectoryItem = ({ category }) => {
  const { imgUrl, title } = category;
  const navigate = useNavigate();

  const goToCategory = () => {
    navigate(`/shop/${title.toLowerCase()}`);
    window.scrollTo(0, 0);
  };
  return (
    <DirectoryItemContainer onClick={goToCategory}>
      <BackgroundImage imageUrl={imgUrl} />
      <Body>
        <h2>{title}</h2>
        <p>Shop now</p>
      </Body>
    </DirectoryItemContainer>
  );
};

export default DirectoryItem;

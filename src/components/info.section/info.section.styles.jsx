import styled from "styled-components";

export const Info = styled.div`
  display: block;
  gap: 10px;
`;
export const InfoContainer = styled.div`
  height: 300px;
  border-radius: 0.8rem;
  display: flex;
  flex-wrap: wrap;
  justify-content: space-around;
  align-items: center;
  padding: 25px;
  h2 {
    font-weight: 500;
    font-size: 20px;
  }
  h3 {
    font-weight: 300;
    font-size: 15px;
  }

  img {
    width: 85px;
    height: 85px;
    margin-right: 10px;
  }
  div {
    width: 330px;
    height: 250x;
    align-items: center;
    justify-content: space-between;
    display: flex;
  }
`;

export const Stripe = styled.div`
  height: 2px;
  background-color: #e9dac1;
  margin: 20px;
`;

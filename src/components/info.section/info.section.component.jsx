import "./info.section.styles.jsx";
import life from "../../assets/life.png";
import delivery from "../../assets/fast-delivery.png";
import support from "../../assets/support.png";
import { Info, InfoContainer, Stripe } from "./info.section.styles.jsx";

const InfoSection = () => {
  return (
    <>
      <Stripe />
      <InfoContainer>
        <div>
          <img src={life}></img>
          <Info>
            <h2>Best quality</h2>
            <h3>We do our best to find the most natural products.</h3>
          </Info>
        </div>
        <div>
          <img src={delivery}></img>
          <Info>
            <h2>Fast delivery</h2>
            <h3>Fast and safe delivery. Free over $100.</h3>
          </Info>
        </div>
        <div>
          <img src={support}></img>
          <Info>
            <h2>Great support</h2>
            <h3>A team of experts passionate about product management.</h3>
          </Info>
        </div>
      </InfoContainer>
      <Stripe />
    </>
  );
};

export default InfoSection;

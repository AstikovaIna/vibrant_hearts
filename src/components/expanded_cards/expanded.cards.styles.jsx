import styled from "styled-components";
import { Container } from "./cards.styles";

export const PanelActive = styled.div`
  flex: 5;

  background-image: ${({ imageUrl }) => `url(${imageUrl})`};
  h3 {
    opacity: 1;
    transition: opacity 0.3s ease-in 0.4s;
  }
`;
export const Panel = styled.div`
  background-size: cover;
  background-position: center;
  background-repeat: no-repeat;
  background-image: ${({ imageUrl }) => `url(${imageUrl})`};
  height: 80vh;
  border-radius: 1rem;
  color: aliceblue;
  cursor: pointer;
  flex: 0.5;
  margin: 10px;
  position: relative;
  transition: flex 0.7s ease-in;

  h3 {
    font-size: 20px;
    position: absolute;
    bottom: 20px;
    margin: 0;
    opacity: 0;
      }
  }
`;

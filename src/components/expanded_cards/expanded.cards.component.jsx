import "./expanded.cards.styles.scss";
import { useState } from "react";
import React from "react";

const ExpandedCards = ({ card }) => {
  const [isActive, setActive] = useState(true);

  const handleClick = () => {
    setActive(false);
  };

  const unClick = () => {
    setActive(true);
  };
  const { imgUrl, title } = card;

  return isActive ? (
    <div
      className="panel"
      style={{
        backgroundImage: `url(${imgUrl})
    `,
      }}
      onClick={handleClick}
    >
      <h3>{title}</h3>
    </div>
  ) : (
    <div
      className="panel active"
      style={{ backgroundImage: `url(${imgUrl})` }}
      onClick={unClick}
    >
      <h3>{title}</h3>
    </div>
  );
};

export default ExpandedCards;

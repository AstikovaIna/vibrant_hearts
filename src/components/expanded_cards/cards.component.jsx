import "./cards.styles.scss";
import { cards } from "../../utils/expanded.cards";
import ExpandedCards from "./expanded.cards.component";

const Cards = () => {
  return (
    <>
      <div className="gift-ideas" id="cards">
        <h2>Explore our ideas</h2>
        <div className="orange-underline"></div>
      </div>
      <div className="container">
        {cards.map((card) => (
          <ExpandedCards key={card.id} card={card} />
        ))}
      </div>
    </>
  );
};

export default Cards;

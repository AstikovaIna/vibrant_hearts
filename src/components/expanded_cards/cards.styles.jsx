import styled from "styled-components";

export const OrangeUnderline = styled.div`
  width: 70px;
  height: 2px;
  background-color: #f76e11;
`;
export const GiftIdeas = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  flex-direction: column;

  h2 {
    font-weight: 500;
    font-size: 35px;
    margin-bottom: 0;
  }
`;

export const Container = styled.div`
  display: flex;
  width: 90vw;
  height: 700px;
  align-items: center;
  justify-content: center;

  h2 {
    font-weight: 500;
    font-size: 35px;
    margin-bottom: 0;
  }
  // @media (max-width: 480px) {
  //   width: 100vw;
  // }
`;

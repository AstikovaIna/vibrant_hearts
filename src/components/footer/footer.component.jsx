import { useSelector } from "react-redux";
import { firstLetterToUpperCase } from "../../utils/firstLetterToUpperCase";
import { selectCategoriesMap } from "../../store/categories/category.selector";
import { Link, useNavigate } from "react-router-dom";
import {categories} from '../../utils/categories'
import logo from "../../assets/VH-logo.svg";
import {
  FooterContainer,
  FooterBottom,
  Slogan,
  TextSlogan,
  MainContainer,
} from "./footer.styles.jsx";

const Footer = () => {
  const categoriesMap = useSelector(selectCategoriesMap);
  const scrollToTop = () => {
    window.scrollTo(0, 0);
  };
  const navigate = useNavigate();
  const navigateToProducts = () => {
    navigate(`/shop`);
    scrollToTop();
  };
  
  return (
    <>
      <MainContainer>
        <FooterContainer>
          <Slogan onClick={scrollToTop}>
            <img src={logo} alt="logo"></img>

            <TextSlogan>We make your everyday life more special.</TextSlogan>
          </Slogan>
          <div className="contact">
            <h3>Contact</h3>
            <h4>59 Tzahal str., Tel-Aviv, Israel</h4>
            <h4>vibrant@hearts.com</h4>
            <h4>+972 535 322 866</h4>
          </div>
          <div className="products">
            <h3 onClick={navigateToProducts}>Products</h3>
            <>
              {categories.map((category) => {
                const categoryTitle = firstLetterToUpperCase(category.title);
                const path = category.title.toLocaleLowerCase();
                return (
                  <h4 key={category.id}>
                    <Link to={`/shop/${path}`} onClick={scrollToTop}>
                      {categoryTitle}
                    </Link>
                  </h4>
                );
              })}
            </>
          </div>
          <div className="about">
            <h3>About</h3>
            <h4>
              <Link to="#">Who we are</Link>
            </h4>
            <h4>
              <Link to="#">Service</Link>
            </h4>
            <h4>
              <Link to="#">Payment methods</Link>
            </h4>
          </div>
        </FooterContainer>
      </MainContainer>
      <FooterBottom>
        <h4>Developed by StudioV² © All rights reserved.</h4>
      </FooterBottom>
    </>
  );
};

export default Footer;

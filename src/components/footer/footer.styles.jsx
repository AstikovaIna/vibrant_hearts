import styled from "styled-components";

export const FooterBottom = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  h4{
  font-weight: 300;
  font-size: 15px;
`;

export const Slogan = styled.div`
  width: 100px;
  height: 100px;
  cursor: pointer;
  margin-bottom: 50px;
`;
export const TextSlogan = styled.h4`
  color: #f76e11;
  font-weight: 350;
  font-size: 20px;
  margin-left: 9px;
`;

export const FooterContainer = styled.div`
  height: 400px;
  border: solid #ff4a4a 1px;
  border-radius: 16% 84% 57% 43% / 62% 56% 44% 38%;
  display: flex;
  flex-wrap: wrap;
  justify-content: space-around;
  align-items: center;

  margin-top: 40px;
  margin-bottom: 10px;

  h3 {
    font-weight: 400;
    font-size: 20px;
    cursor: pointer;
    &:hover {
      color: #f76e11;
    }
  }
  h4 {
    font-weight: 350;
    font-size: 17px;
    cursor: pointer;
  }
  a:hover {
    color: #f76e11;
  }
  @media (max-width: 640px) {
    flex-direction: column-reverse;
  }
`;
export const MainContainer = styled.div`
  border: solid #f7ecde 3px;

  border-radius: 61% 39% 41% 59% / 46% 30% 70% 54%;
  @media (max-width: 768px) {
    margin-left: 20px;
    margin-right: 20px;
  }
`;

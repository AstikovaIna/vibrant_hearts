import {
  CartItemContainer,
  ItemDetails,
  ItemTitle,
  QuantityAndPrice,
} from "./cart-item.styles.jsx";

const CartItem = ({ cartItem }) => {
  const { name, imageUrl, price, quantity } = cartItem;
  return (
    <CartItemContainer>
      <img src={imageUrl} alt={name} />
      <ItemDetails>
        <ItemTitle><h3>{name}</h3></ItemTitle>
        <QuantityAndPrice>
          {quantity} x ${price}
        </QuantityAndPrice>
      </ItemDetails>
    </CartItemContainer>
  );
};

export default CartItem;

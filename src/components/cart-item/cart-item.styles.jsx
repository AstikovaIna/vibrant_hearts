import styled from "styled-components";

export const CartItemContainer = styled.div`
  width: 100%;
  display: flex;
  height: 80px;
  margin-bottom: 15px;
justify-content: center;
  img {
    width: 30%;
  }
`;
export const ItemDetails = styled.div`
  width: 70%;
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  justify-content: space-around;
  padding: 10px 20px;
`;
export const ItemTitle = styled.div`
display: flex;
flex-wrap: wrap;
padding-right: 10px;
  h3{
    font-size: 16px;
  }
`;
export const QuantityAndPrice = styled.span`
  font-size: 14px;
  padding-top: 5px;
  font-weight: 700;
`;

import styled, { keyframes } from "styled-components";

export const HappyClients = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  flex-direction: column;
  margin-top: 60px;
  margin-bottom: 30px;

  h2 {
    font-weight: 500;
    font-size: 35px;
    margin-bottom: 0;
  }
  div {
    width: 70px;
    height: 3px;
    background-color: #f76e11;
  }
`;
const Grow = keyframes`
0%{
    transform: scaleX(0);
  }
`;
export const LeftQuotes = styled.div`
  font-size: 1.5vw;
  position: absolute;
  top: 70px;
  right: 40px;
  @media (max-width: 768px) {
    display: none;
  } ;
`;
export const RightQuotes = styled.div`
  font-size: 1.5vw;
  position: absolute;
  top: 70px;
  left: 40px;
  @media (max-width: 768px) {
    display: none;
  } ;
`;
export const ProgressBar = styled.div`
  background-color: #ff7b54;
  border-radius: 2rem;
  width: 100%;
  height: 2px;
  margin-bottom: 20px;
  animation: ${Grow} 10s linear infinite;
  transform-origin: left;
`;
export const TestimonialStyle = styled.p`
  line-height: 28px;
  text-align: justify;
  padding-bottom: 15px;
`;

export const User = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  h3 {
    margin: 0;
  }
  p {
    font-weight: normal;
    margin: 10px 0;
  }
  img {
    border-radius: 61% 39% 28% 72% / 37% 30% 70% 63%;
    height: 75px;
    width: 75px;
  }
  div {
    margin-left: 10px;
  }
`;
export const TestimonialContainer = styled.div`
  background-color: #fff5e4;
  border-radius: 1rem;
  flex-direction: column;
  margin: 30px auto 60px auto;

  padding: 40px 80px;
  max-width: 768px;
  position: relative;
  @media (max-width: 768px) {
    padding: 20px 30px;
    margin-left: 20px;
    margin-right: 20px;
  }
`;

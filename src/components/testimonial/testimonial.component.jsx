import { useState, useEffect } from "react";
import {
  HappyClients,
  LeftQuotes,
  RightQuotes,
  ProgressBar,
  TestimonialStyle,
  TestimonialContainer,
  User,
} from "./testimonial.styles.jsx";
import { testimonials } from "../../utils/testimonials";
import { FaQuoteLeft } from "react-icons/fa";
import { FaQuoteRight } from "react-icons/fa";

const Testimonial = () => {
  const [testimonial, setTestimonial] = useState();

  useEffect(() => {
    const testimonialLoop = () => {
      testimonials.map((item, index) => {
        if (index < testimonials.length) {
          setTimeout(() => {
            setTestimonial(item);
          }, (index + 1) * 10000);
        }
      });
    };
    setInterval(testimonialLoop(), 5000);
  }, []);
  return (
    <>
      <HappyClients>
        <h2>Our happy clients</h2>
        <div className="orange-underline"></div>
      </HappyClients>
      <TestimonialContainer>
        {testimonial && (
          <>
            <ProgressBar />
            <RightQuotes>
              <FaQuoteRight />
            </RightQuotes>
            <LeftQuotes>
              <FaQuoteLeft />
            </LeftQuotes>
            <TestimonialStyle>{testimonial.text}</TestimonialStyle>
            <User>
              <img src={testimonial.image} alt="user-image" />
              <div>
                <h3>{testimonial.name}</h3>
                <p>{testimonial.position}</p>
              </div>
            </User>
          </>
        )}
      </TestimonialContainer>
    </>
  );
};

export default Testimonial;

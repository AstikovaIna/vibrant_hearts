import { StyledBurger, StyledMenu } from "./burger.style";

export const Menu = ({ open }) => {
  return (
    <StyledMenu open={open}>
      <a href="/">Home</a>
      <a href="/shop">Shop</a>
      <a href="/">About</a>
      <a href="/">Contact</a>
    </StyledMenu>
  );
};

export const Burger = ({ open, setOpen }) => {
  const handleClick = () => {
    setOpen(!open);
  };
  return (
    <StyledBurger open={open} onClick={handleClick}>
      <div />
      <div />
      <div />
    </StyledBurger>
  );
};

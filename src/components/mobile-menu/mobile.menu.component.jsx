import { useSelector } from "react-redux";
import { useState } from "react";
import { selectIsCartOpen } from "../../store/cart/cart.selector";
import { Menu, Burger } from "./burger.component";
import { BurgerContainer, RightContainer } from "./burger.style";
import { Link } from "react-router-dom";
import { selectCurrentUser } from "../../store/user/user.selector";
import CartDropdown from "../cart-dropdown/cart-dropdown.component";
import CartIcon from "../cart-icon/cart-icon.components";
import { signOutUser } from "../../firebase/firebase";
import { AiOutlineLogin } from "react-icons/ai";
import { AiOutlineLogout } from "react-icons/ai";

const BurgerMenu = () => {
  const [open, setOpen] = useState(false);
  const  isCartOpen  = useSelector(selectIsCartOpen);
  const currentUser = useSelector(selectCurrentUser);

  return (
    <BurgerContainer>
      <div>
        <Menu open={open} setOpen={setOpen} />
        <Burger open={open} setOpen={setOpen} />
      </div>
      <RightContainer>
        <CartIcon />
        {isCartOpen && <CartDropdown />}
        {currentUser ? (
          <>
            <Link onClick={signOutUser}>
              <AiOutlineLogout />
            </Link>
          </>
        ) : (
          <Link to="/auth">
            <AiOutlineLogin />
          </Link>
        )}
      </RightContainer>
    </BurgerContainer>
  );
};

export default BurgerMenu;

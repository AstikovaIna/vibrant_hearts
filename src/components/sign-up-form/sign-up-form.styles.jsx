import styled from "styled-components";

export const SignUpContainer = styled.div`
  display: flex;
  flex-direction: column;
  width: 380px;
  background-color: #fdeedc;
  padding: 30px;
  border-radius: 30% 70% 73% 27% / 52% 66% 44% 48%;

  h3 {
    margin: 10px 0;
  }
`;

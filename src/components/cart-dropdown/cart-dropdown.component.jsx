import { useSelector, useDispatch } from "react-redux";
import { useNavigate } from "react-router-dom";
import Button from "../button/button.component";
import CartItem from "../cart-item/cart-item.component";
import { setIsCartOpen } from "../../store/cart/cart.action";
import { selectCartItems, selectIsCartOpen } from "../../store/cart/cart.selector";
import {
  CartDropdownContainer,
  EmptyMessage,
  CartItemsContainer,
} from "./cart-dropdown.styles.jsx";

const CartDropdown = () => {
  
  const cartItems = useSelector(selectCartItems)
  const isCartOpen = useSelector(selectIsCartOpen)
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const toggleIsCartOpen = () => dispatch(setIsCartOpen(!isCartOpen));
  const goToCheckout = () => {
    navigate("/checkout");
    toggleIsCartOpen();
  };
  return (
    <CartDropdownContainer>
      <CartItemsContainer>
        {cartItems.length ? (
          cartItems.map((item) => <CartItem key={item.id} cartItem={item} />)
        ) : (
          <EmptyMessage>Your cart is empty!</EmptyMessage>
        )}
      </CartItemsContainer>

      <Button onClick={goToCheckout}>Go to cart</Button>
    </CartDropdownContainer>
  );
};

export default CartDropdown;

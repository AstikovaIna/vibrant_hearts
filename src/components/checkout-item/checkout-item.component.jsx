import {useSelector, useDispatch} from 'react-redux';

import { selectCartItems } from '../../store/cart/cart.selector.js';
import { addItemToCart, removeItemFromCart, clearItemFromCart } from '../../store/cart/cart.action.js';

import {
  CheckoutItemContainer,
  ImageContainer,
  Quantity,
  Price,
  RemoveButton,
  Arrow,
  Name,
} from "./checkout-item.styles.jsx";
import { MdOutlineDeleteForever } from "react-icons/md";
import { RiArrowRightSFill } from "react-icons/ri";
import { RiArrowLeftSFill } from "react-icons/ri";

const CheckoutItem = ({ cartItem }) => {
  const { name, imageUrl, price, quantity, id } = cartItem;
  const cartItems = useSelector(selectCartItems);
  const dispatch = useDispatch();

  const clearItemHandler = () => dispatch(clearItemFromCart(cartItems, cartItem));

  const addItemHandler = () => dispatch(addItemToCart(cartItems, cartItem));
  const removeItemHandler = () => dispatch(removeItemFromCart(cartItems, cartItem));
  return (
    <CheckoutItemContainer>
      <ImageContainer>
        <img src={imageUrl} alt={name} />
      </ImageContainer>
      <Name to={`/product-details/${cartItem.id}`}>{name}</Name>
      <Quantity>
        <Arrow onClick={removeItemHandler}>
          <RiArrowLeftSFill />
        </Arrow>
        <span className="value">{quantity}</span>
        <Arrow onClick={addItemHandler}>
          <RiArrowRightSFill />
        </Arrow>
      </Quantity>
      <Price>${price}</Price>
      <RemoveButton onClick={clearItemHandler}>
        <MdOutlineDeleteForever />
      </RemoveButton>
    </CheckoutItemContainer>
  );
};

export default CheckoutItem;

import styled from "styled-components";
import { Link } from "react-router-dom";
import { randomRadius } from "../../utils/border-radius";

export const CheckoutItemContainer = styled.div`
  width: 90vw;
  display: flex;
  min-height: 100px;
  border-bottom: 1px solid darkgrey;
  padding: 15px 0;
  margin-right: 15px;
  font-size: 18px;
  align-items: center;
`;
export const ImageContainer = styled.div`
  width: 20%;
  padding-right: 15px;

  img {
    width: 100%;
    height: 100%;
    border-radius: ${randomRadius};
  }
`;
export const Name = styled(Link)`
  margin: 0 10px;
  width: 23%;
  cursor: pointer;
`;
export const Price = styled.span`
  width: 23%;
`;

export const Quantity = styled.span`
  display: flex;
  width: 23%;
  margin: 0 10px;
`;

export const Arrow = styled.div`
  cursor: pointer;
`;

export const RemoveButton = styled.div`
  padding-left: 12px;
  cursor: pointer;
`;

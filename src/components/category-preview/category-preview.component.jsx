import {
  CategoryPreviewContainer,
  TitleContainer,
  Title,
  Preview,
} from "./category-preview.styles.jsx";
import ProductCard from "../product-card/product-card.component";
import { firstLetterToUpperCase } from "../../utils/firstLetterToUpperCase.js";

const CategoryPreview = ({ title, products }) => {
  const capitalizedFirstLetterTitle = firstLetterToUpperCase(title);

  return (
    <CategoryPreviewContainer>
      <TitleContainer>
        <Title to={title}>{capitalizedFirstLetterTitle}</Title>
        <div></div>
      </TitleContainer>
      <Preview>
        {products
          .filter((_, index) => index < 4)
          .map((product) => (
            <ProductCard key={product.id} product={product} />
          ))}
      </Preview>
    </CategoryPreviewContainer>
  );
};

export default CategoryPreview;

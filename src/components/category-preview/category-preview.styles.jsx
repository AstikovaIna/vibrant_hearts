import styled from "styled-components";
import { Link } from "react-router-dom";

export const CategoryPreviewContainer = styled.div`
  display: flex;
  flex-direction: column;
  margin: 35px 25px 15px 25px;
`;
export const TitleContainer = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  flex-direction: column;
  margin-bottom: 35px;
  div {
    width: 70px;
    height: 2px;
    background-color: #f76e11;
  }
`;

export const Title = styled(Link)`
  font-size: 28px;
  font-weight: 500;
  margin-top: 40px;
  // margin-bottom: 25px;
  margin-left: 25px
  cursor: pointer;
`;

export const Preview = styled.div`
  display: grid;
  grid-template-columns: repeat(4, 1fr);
  column-gap: 20px;
  @media (max-width: 640px) {
    grid-template-columns: repeat(2, 1fr);
    row-gap: 25px;
  }
`;

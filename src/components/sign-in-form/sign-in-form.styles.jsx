import styled from "styled-components";

export const ButtonContainer = styled.div`
  display: flex;
  justify-content: space-between;
`;

export const SignInContainer = styled.div`
  display: flex;
  flex-direction: column;
  width: 380px;
  background-color: #f7ecde;
  padding: 30px;
  border-radius: 74% 26% 35% 65% / 24% 49% 51% 76%;
  h3 {
    margin: 10px 0;
  }
`;

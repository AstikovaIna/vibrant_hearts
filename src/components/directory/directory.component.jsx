import DirectoryItem from "../directory-item/directory-item.component";
import { Catalog, DirectoryContainer } from "./directory.styles.jsx";

const Directory = ({ categories }) => {
  return (
    <>
      <Catalog>
        <h2>Products catalog</h2>
        <div></div>
      </Catalog>
      <DirectoryContainer>
        {categories.map((category) => (
          <DirectoryItem key={category.id} category={category} />
        ))}
      </DirectoryContainer>
    </>
  );
};

export default Directory;

import styled from "styled-components";

export const Catalog = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  flex-direction: column;
  margin-top: 80px;
  margin-bottom: 35px;

  h2 {
    font-weight: 500;
    font-size: 35px;
    margin-bottom: 0;
  }
  div {
    width: 70px;
    height: 2.5px;
    background-color: #f76e11;
  }
`;
export const DirectoryContainer = styled.div`
  display: flex;
  flex-wrap: wrap;
  justify-content: space-between;
  @media (max-width: 740px) {
    margin: 10px;
  }
`;

import {configureStore} from "@reduxjs/toolkit"
import { rootReducer } from "./root-reducer";
import { persistStore, persistReducer } from "redux-persist";
import storage from "redux-persist/lib/storage";
// import { loggerMiddleWare } from "./middleware/logger";
import logger from "redux-logger";
import thunk from "redux-thunk";

const persistConfig = {
    key: 'root',
    storage,
    whitelist: ['cart'],
}

const persistedReducer = persistReducer(persistConfig, rootReducer)

export const store = configureStore({
    reducer: persistedReducer,
    middleware: [process.env.NODE_ENV !== 'production' && logger, thunk,].filter(Boolean)
    // middleware: [loggerMiddleWare]
})

export const persistor = persistStore(store)
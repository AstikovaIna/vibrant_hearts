import Navigation from "../../components/navigation/navigation.component";
import Landing from "../../components/landing/landing.component";
import InfoSection from "../../components/info.section/info.section.component";
import Directory from "../../components/directory/directory.component";
import Footer from "../../components/footer/footer.component";
import Cards from "../../components/expanded_cards/cards.component";
import Testimonial from "../../components/testimonial/testimonial.component";
import { categories } from "../../utils/categories";

const Home = () => {
  return (
    <>
      <Navigation />
      <Landing />
      <Directory categories={categories} />
      <InfoSection />
      <Cards />
      <Testimonial />
      <Footer />
    </>
  );
};

export default Home;

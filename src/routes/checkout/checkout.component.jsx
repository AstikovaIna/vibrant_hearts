import { useSelector } from "react-redux";
import { selectCartItems, selectCartTotal } from "../../store/cart/cart.selector";
import Navigation from "../../components/navigation/navigation.component";
import CheckoutItem from "../../components/checkout-item/checkout-item.component";
import InfoSection from "../../components/info.section/info.section.component";
import Footer from "../../components/footer/footer.component";
import PaymentForm from "../../components/payment-form/payment-form.component";
import {
  Catalog,
  CheckoutContainer,
  Header,
  CheckoutHeader,
  Total,
  OrangeUnderline,
} from "./checkout.styles.jsx";

const Checkout = () => {
  
  const cartItems = useSelector(selectCartItems);
  const cartTotal = useSelector(selectCartTotal)
  const columnDescription = ["Product", "Qty.", "Price", "Remove"];
  return (
    <>
      <Navigation />
      <Catalog>
        <h2>Your products</h2>
        <OrangeUnderline />
      </Catalog>
      <CheckoutContainer>
        <CheckoutHeader>
          {columnDescription.map((item, id) => (
            <Header key={id}>
              <span>{item}</span>
            </Header>
          ))}
        </CheckoutHeader>

        {cartItems.map((cartItem) => (
          <CheckoutItem key={cartItem.id} cartItem={cartItem} />
        ))}
        <Total>Total: ${cartTotal}</Total>
        <PaymentForm/>
      </CheckoutContainer>
      <InfoSection />
      <Footer />
    </>
  );
};

export default Checkout;

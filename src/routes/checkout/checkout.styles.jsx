import styled from "styled-components";

export const OrangeUnderline = styled.div`
  width: 70px;
  height: 2px;
  background-color: #f76e11;
`;
export const Catalog = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  flex-direction: column;
  margin-top: 80px;
  margin-bottom: 35px;

  h2 {
    font-weight: 500;
    font-size: 35px;
    margin-bottom: 0;
  }
`;

export const Total = styled.span`
  margin-top: 30px;
  margin-left: auto;
  font-size: 25px;
`;

export const Header = styled.div`
  text-transform: capitalize;
  span {
    justify-content: center;
  }
  @media (max-width: 412px) {
    width: 80vw;
  }
`;
export const CheckoutHeader = styled.div`
  width: 90vw;
  padding: 10px 0; 
  display: flex;
  align-items: center;
  justify-content: space-between;
  border-bottom: 1px solid darkgrey;

  @media (max-width: 412px) {
    display: none;
  }
`;
export const CheckoutContainer = styled.div`
  width: 100%;
  display: flex;
  flex-direction: column;
  align-items: center;
  margin-top: 50px
  margin-bottom: 80px;

  @media (max-width: 768px) {
    width: 100%;
  }
`;

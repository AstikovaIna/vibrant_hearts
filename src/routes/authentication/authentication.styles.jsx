import styled from "styled-components";

export const AuthContainer = styled.div`
  display: flex;
  flex-wrap: wrap;
  width: 90vw;
  justify-content: space-around;
  margin: 50px auto;
`;

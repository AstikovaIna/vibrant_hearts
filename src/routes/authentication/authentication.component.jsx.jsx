import Navigation from "../../components/navigation/navigation.component";
import SignUpForm from "../../components/sign-up-form/sign-up-form.component";
import SignInForm from "../../components/sign-in-form/sign-in-form.component";
import InfoSection from "../../components/info.section/info.section.component";
import Footer from "../../components/footer/footer.component";
import { AuthContainer } from "./authentication.styles.jsx";

const Authentication = () => {
  return (
    <>
      <Navigation />
      <AuthContainer>
        <SignInForm />
        <SignUpForm />
      </AuthContainer>
      <InfoSection />
      <Footer />
    </>
  );
};

export default Authentication;

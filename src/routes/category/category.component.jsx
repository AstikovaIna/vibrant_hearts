import { useState, useEffect } from "react";
import { useSelector } from "react-redux";
import { useParams } from "react-router-dom";
import { selectCategoriesMap, selectIsLoading } from "../../store/categories/category.selector";
import Spinner from "../../components/spinner/spinner.component";
import ProductCard from "../../components/product-card/product-card.component";
import Navigation from "../../components/navigation/navigation.component";
import InfoSection from "../../components/info.section/info.section.component";
import Footer from "../../components/footer/footer.component";
import { firstLetterToUpperCase } from "../../utils/firstLetterToUpperCase";
import {
  CategoryContainer,
  CategoryTitle,
  TitleContainer,
  OrangeUnderline,
} from "./category.styles.jsx";

const Category = () => {
  const { category } = useParams();
  const categoriesMap = useSelector(selectCategoriesMap);
  const isLoading = useSelector(selectIsLoading)
  const [products, setProducts] = useState(categoriesMap[category]);
  

  useEffect(() => {
    
    setProducts(categoriesMap[category]);
  }, [category, categoriesMap]);

  const categoryFirstLetterToUpperCase = firstLetterToUpperCase(category);

  return (
    <>
      <Navigation />
      <TitleContainer>
        <CategoryTitle>{categoryFirstLetterToUpperCase}</CategoryTitle>
        <OrangeUnderline />
      </TitleContainer>
      {isLoading ? (
        <Spinner/>
      ) : (

      <CategoryContainer>
        {products &&
          products.map((product) => (
            <ProductCard
              key={product.id}
              product={product}
              category={category}
            />
          ))}
      </CategoryContainer>
      )}
      <InfoSection />
      <Footer />
    </>
  );
};

export default Category;

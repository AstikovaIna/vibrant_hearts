import styled from "styled-components";

export const CategoryContainer = styled.div`
  display: grid;
  grid-template-columns: repeat(4, 1fr);
  column-gap: 25px;
  row-gap: 65px;
  margin: 35px 25px 15px 25px;
  @media (max-width: 640px) {
    grid-template-columns: repeat(2, 1fr);
  }
`;
export const OrangeUnderline = styled.div`
  width: 70px;
  height: 2px;
  margin-left: 25px;
  background-color: #f76e11;
`;
export const CategoryTitle = styled.h2`
  font-size: 28px;
  font-weight: 500;
  margin: 40px 0 0px 25px;
`;
export const TitleContainer = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  flex-direction: column;
  margin-bottom: 35px;
`;

import styled from "styled-components";

export const ProductsContainer = styled.div`
  display: grid;
  grid-template-columns: repeat(4, 1fr);
  column-gap: 25px;
  row-gap: 65px;
  margin: 35px 25px 15px 25px;
`;
// .products-container{
//   display: grid;
//   grid-template-columns: repeat(4, 1fr);
//   column-gap: 25px;
//   row-gap: 65px;
//   margin: 35px 25px 15px 25px;

// }

// h2{
//   margin-left: 25px;

// }

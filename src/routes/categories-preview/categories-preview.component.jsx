import { useSelector } from "react-redux";
import { selectCategoriesMap, selectIsLoading } from "../../store/categories/category.selector";
import Navigation from "../../components/navigation/navigation.component";
import CategoryPreview from "../../components/category-preview/category-preview.component";
import Footer from "../../components/footer/footer.component";
import InfoSection from "../../components/info.section/info.section.component";
import Spinner from "../../components/spinner/spinner.component"

const CategoriesPreview = () => {
  const categoriesMap = useSelector(selectCategoriesMap);
  const isLoading = useSelector(selectIsLoading)
  return (

    <>
      <Navigation />
      {isLoading ? (
        <Spinner/>
      ) : (

      <div>
        {Object.keys(categoriesMap).map((title) => {
          const products = categoriesMap[title];
          return (
            <CategoryPreview key={title} title={title} products={products} />
          );
        })}
      </div>
      )}
      <InfoSection />
      <Footer />
    </>
  );
};

export default CategoriesPreview;

export const testimonials = [
  {
    name: "Rene Silver",
    position: "Administrator",
    image: "https://randomuser.me/api/portraits/women/38.jpg",
    text: "Velit scelerisque in dictum non consectetur a erat nam at. Turpis tincidunt id aliquet risus. Arcu felis bibendum ut tristique et egestas quis ipsum. Sollicitudin tempor id eu nisl nunc mi ipsum. ",
  },
  {
    name: "Steve Jameson",
    position: "Designer",
    image: "https://randomuser.me/api/portraits/men/33.jpg",
    text: "Suspendisse interdum consectetur libero id faucibus nisl. Vestibulum mattis ullamcorper velit sed ullamcorper morbi tincidunt ornare massa. ",
  },
  {
    name: "Sveta Belinski",
    position: "Teacher",
    image: "https://randomuser.me/api/portraits/women/43.jpg",
    text: "In massa tempor nec feugiat nisl pretium. Tellus integer feugiat scelerisque varius morbi enim nunc faucibus a. Mi tempus imperdiet nulla malesuada pellentesque elit. Sagittis id consectetur purus ut faucibus pulvinar. ",
  },
  {
    name: "Gabi Myles",
    position: "Engineer",
    image: "https://randomuser.me/api/portraits/men/43.jpg",
    text: "In massa tempor nec feugiat nisl pretium. Tellus integer feugiat scelerisque varius morbi enim nunc faucibus a. Mi tempus imperdiet nulla malesuada pellentesque elit. Sagittis id consectetur purus ut faucibus pulvinar. ",
  },
  {
    name: "Sky Donsen",
    position: "Farmer",
    image: "https://randomuser.me/api/portraits/women/23.jpg",
    text: "In massa tempor nec feugiat nisl pretium. Tellus integer feugiat scelerisque varius morbi enim nunc faucibus a. Mi tempus imperdiet nulla malesuada pellentesque elit. Sagittis id consectetur purus ut faucibus pulvinar. ",
  },
  {
    name: "Anne Fritz",
    position: "Pharmacy",
    image: "https://randomuser.me/api/portraits/women/25.jpg",
    text: "Tellus integer feugiat scelerisque varius morbi enim nunc faucibus a. Mi tempus imperdiet nulla elit. Sagittis id consectetur purus ut faucibus pulvinar. ",
  },
  {
    name: "Maya Harper",
    position: "Musician",
    image: "https://randomuser.me/api/portraits/women/26.jpg",
    text: "Beshak jslia herrby haid. Tellus integer feugiat scelerisque varius morbi enim nunc faucibus a. Mi tempus imperdiet nulla elit. Sagittis id consectetur purus ut faucibus pulvinar. ",
  },
];
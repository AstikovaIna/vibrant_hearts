export const categories = [
  {
    id: 1,
    title: "Gift Boxes",
    imgUrl:
      "https://cdn.shopify.com/s/files/1/0049/5304/7158/products/20210826_095506_1024x1024.jpg?v=1629973657",
  },
  {
    id: 2,
    title: "Textile",
    imgUrl:
      "https://makomstudio.co.il/wp-content/uploads/2021/07/WhatsApp-Image-2021-07-12-at-19.24.59-1.jpeg",
  },
  {
    id: 3,
    title: "for Home",
    imgUrl:
      "https://makomstudio.co.il/wp-content/uploads/2021/10/WhatsApp-Image-2021-10-12-at-13.36.14.jpeg",
  },
  {
    id: 4,
    title: "Atmosphere",
    imgUrl:
      "https://makomstudio.co.il/wp-content/uploads/2021/07/ADFB842D-1B94-4CB7-BF19-1DD1BF0A75CC-scaled.jpeg",
  },
  {
    id: 5,
    title: "Wood products",
    imgUrl:
      "https://makomstudio.co.il/wp-content/uploads/2021/02/WhatsApp-Image-2021-02-21-at-10.17.21-1.jpeg",
  },
  {
    id: 6,
    title: "Soaps",
    imgUrl:
      "https://makomstudio.co.il/wp-content/uploads/2020/12/File_014_.jpg",
  },
];
import interior from "../assets/images/interior.jpg";
import balloons from "../assets/images/balloons.jpg";
import bathBox from "../assets/images/bath_products.jpg";
import candles from "../assets/images/candles.jpg";
import coffee from "../assets/images/coffee.jpg";
import cottonPads from "../assets/images/cotton_pads.jpg";
import crystals from "../assets/images/crystals.jpg";
import soaps from "../assets/images/soaps2.jpg";

export const cards = [
  {
    id: 1,
    imgUrl: interior,
    title: "Interior",
  },

  {
    id: 2,
    imgUrl: balloons,
    title: "balloons",
  },
  {
    id: 3,
    imgUrl: bathBox,
    title: "Bath box",
  },
  {
    id: 4,
    imgUrl: candles,
    title: "Candles",
  },
  {
    id: 5,
    imgUrl: coffee,
    title: "Coffee",
  },
  {
    id: 6,
    imgUrl: cottonPads,
    title: "Cotton pads",
  },
  {
    id: 7,
    imgUrl: crystals,
    title: "Crystals",
  },
  {
    id: 8,
    imgUrl: soaps,
    title: "Soaps",
  },
];
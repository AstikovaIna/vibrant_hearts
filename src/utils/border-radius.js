const borderRadius = [
  "42% 58% 70% 30% / 23% 49% 51% 77%",
  "56% 44% 78% 22% / 34% 48% 52% 66%",
  "44% 56% 78% 22% / 34% 74% 26% 66% ",
  "44% 56% 35% 65% / 34% 49% 51% 66% ",
  "74% 26% 35% 65% / 34% 49% 51% 66% ",
  "74% 26% 35% 65% / 57% 49% 51% 43% ",
];

export const randomRadius = () =>
  borderRadius[Math.floor(Math.random() * borderRadius.length)];